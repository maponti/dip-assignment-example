# DIP Assignment Example

Image Processing - scc0251. 

Assignment 1 - Image enhancemenet

Authors:
* Moacir A. Ponti
* Gabriel B. Cavallari
* Vinícius M. da Costa


Folders and files:
* [Python code](./submission/dip01_submission.py) contains the Python code used for run.codes submission
* [Images](/images) contains images used in the demos
* [Notebook with Demo](dip01-enhancement.ipynb) is a notebook exemplifying functions developed and submitted



